import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Calculator',
      url: '/calculator',
      icon: 'calculator'
    },
    {
        title: 'Pharmaceuticals',
        url: '/pharmaceuticals',
        icon: 'medkit'
    },
    {
        title: 'Geolocation',
        url: '/geolocation',
        icon: 'pin'
    },
    {
      title: 'Forms',
      url: '/forms',
      icon: 'list-box'
    },
    {
      title: 'Visits',
      url: '/visits',
      icon: 'md-walk'
    },
    {
      title: 'Formations',
      icon: 'ios-school',
      url: '/formations',
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
