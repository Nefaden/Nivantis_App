import { NetSellingPricePopoverComponent } from './../CalculatorPopover/net-selling-price-popover/net-selling-price-popover.component';
import { NetPuchasePicePopoverComponent } from './../CalculatorPopover/net-puchase-pice-popover/net-puchase-pice-popover.component';
import { MultiplierPopoverComponent } from './../CalculatorPopover/multiplier-popover/multiplier-popover.component';
import { Component, OnInit } from '@angular/core';
import { numeric } from 'tar';
import { NumericValueAccessor, PopoverController} from '@ionic/angular';
import { DiscountRatePopoverComponent } from './../CalculatorPopover/discount-rate-popover/discount-rate-popover.component';


@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss'],
})
export class CalculatorPage implements OnInit {

  constructor(private popoverController: PopoverController) {
    console.log('constructed');
  }

  public buttonRoute =
  [
    {
      title: 'Discount rate',
      fonctionLink: 'DiscountRateEvent',
      icon: 'pricetag'
    },
    {
      title: 'Net purchase price',
      fonctionLink: 'NetPurchasePriceEvent',
      icon: 'cash'
    },
    {
      title: 'Net selling price',
      fonctionLink: 'NetSellingPriceEvent',
      icon: 'cart'
    },
    {
      title: 'Multiplier',
      fonctionLink: 'MultiplierEvent',
      icon: 'close'
    }
  ];

  ngOnInit() {
  }

  ButtonRouting(functionName: string, event) {
    switch (functionName) {
      case 'DiscountRateEvent': {
        this.DiscountRateEvent(event);
        break;
      }
      case 'NetPurchasePriceEvent': {
        this.NetPurchasePriceEvent(event);
        break;
      }
      case 'NetSellingPriceEvent': {
        this.NetSellingPriceEvent(event);
        break;
      }
      case 'MultiplierEvent': {
        this.MultiplierEvent(event);
        break;
      }
    }
  }

  DiscountRateEvent(event) {

    this.GetDiscountRatePopoverComponent(event);
  }

  NetPurchasePriceEvent(event) {
    this.GetNetPurchasePricePopoverComponent(event);
  }

  NetSellingPriceEvent(event) {
    this.GetNetSellingPricePopoverComponent(event);
  }

  MultiplierEvent(event) {
    this.GetMultiplierPopoverComponent(event);
  }

  async GetDiscountRatePopoverComponent($event) {
    const popover = await this.popoverController.create({
        component: DiscountRatePopoverComponent,
        cssClass: 'calculator-popover'
    });
    return await popover.present();
  }

  async GetMultiplierPopoverComponent($event) {
    const popover = await this.popoverController.create({
      component : MultiplierPopoverComponent,
      cssClass: 'calculator-popover'
    });
    return await popover.present();
  }

  async GetNetPurchasePricePopoverComponent($event) {
    const popover = await this.popoverController.create({
      component : NetPuchasePicePopoverComponent,
      cssClass: 'calculator-popover'
    });
    return await popover.present();
  }

  async GetNetSellingPricePopoverComponent($event) {
    const popover = await this.popoverController.create({
      component : NetSellingPricePopoverComponent,
      cssClass: 'calculator-popover'
    });
    return await popover.present();
  }
}
