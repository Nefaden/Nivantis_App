import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VisitsPage } from './visits.page';

import { VisitsPopoverComponent } from '../visitsPopover/visitsPopover.component';

const routes: Routes = [
  {
    path: '',
    component: VisitsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [VisitsPopoverComponent],
  declarations: [VisitsPage, VisitsPopoverComponent]
})
export class VisitsPageModule {}
