import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import { VisitsPopoverComponent } from '../visitsPopover/visitsPopover.component';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.page.html',
  styleUrls: ['./visits.page.scss'],
})
export class VisitsPage implements OnInit {
  constructor(private popoverController: PopoverController) {
  }

  ngOnInit() {
    console.log('The component is initialized');
  }

  async visitsPopover() {
    const popover = await this.popoverController.create({
      component: VisitsPopoverComponent,
      cssClass: 'popover_class',
    });
    return await popover.present();
  }
}
