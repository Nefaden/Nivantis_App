import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {PharmaceuticalsPopoverComponent} from '../pharmaceuticalsPopover/pharmaceuticalsPopover.component';

import { IonicModule } from '@ionic/angular';

import { PharmaceuticalsPage } from './pharmaceuticals.page';

const routes: Routes = [
  {
    path: '',
    component: PharmaceuticalsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [PharmaceuticalsPopoverComponent],
  declarations: [PharmaceuticalsPage, PharmaceuticalsPopoverComponent]
})
export class PharmaceuticalsPageModule {}
