import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {PharmaceuticalsPopoverComponent} from '../pharmaceuticalsPopover/pharmaceuticalsPopover.component';

@Component({
  selector: 'app-pharmaceuticals',
  templateUrl: './pharmaceuticals.page.html',
  styleUrls: ['./pharmaceuticals.page.scss'],
})
export class PharmaceuticalsPage implements OnInit {
  constructor(private popoverController: PopoverController) {
  }

  ngOnInit() {
    console.log('The component is initialized');
  }

  async pharmaceuticalsPopover() {
    const popover = await this.popoverController.create({
      component: PharmaceuticalsPopoverComponent,
      cssClass: 'popover_class',
    });
    return await popover.present();
  }
}
