import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import {DownloaderPopoverComponent} from '../downloaderPopover/downloaderPopover.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],

  entryComponents: [DownloaderPopoverComponent],
  declarations: [HomePage, DownloaderPopoverComponent]
})
export class HomePageModule {}
