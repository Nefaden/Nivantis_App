import {Component, OnInit} from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { DownloaderPopoverComponent } from '../downloaderPopover/downloaderPopover.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  constructor(private popoverController: PopoverController) {
  }

  ngOnInit() {
    console.log('The component is initialized');
  }

  async downloaderPopover() {
    const popover = await this.popoverController.create({
      component: DownloaderPopoverComponent,
      cssClass: 'popover_class',
    });
    return await popover.present();
  }
}
