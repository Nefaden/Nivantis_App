import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import { FormationsPopoverComponent } from '../formationsPopover/formationsPopover.component';

@Component({
  selector: 'app-formations',
  templateUrl: './formations.page.html',
  styleUrls: ['./formations.page.scss'],
})
export class FormationsPage implements OnInit {

  constructor(private popoverController: PopoverController) {
  }

  ngOnInit() {
    console.log('The component is initialized');
  }

  async formationsPopover() {
    const popover = await this.popoverController.create({
      component: FormationsPopoverComponent,
      cssClass: 'popover_class',
    });
    return await popover.present();
  }
}
