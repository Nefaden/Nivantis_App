import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FormationsPage } from './formations.page';

import { FormationsPopoverComponent } from '../formationsPopover/formationsPopover.component';

const routes: Routes = [
  {
    path: '',
    component: FormationsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [FormationsPopoverComponent],
  declarations: [FormationsPage, FormationsPopoverComponent]
})
export class FormationsPageModule {}
