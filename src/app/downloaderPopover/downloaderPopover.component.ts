import { Component, OnInit } from '@angular/core';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { File } from '@ionic-native/file/ngx';


@Component({
  selector: 'app-downloader-popover',
  templateUrl: './downloaderPopover.component.html',
  styleUrls: ['./downloaderPopover.component.scss'],
})

export class DownloaderPopoverComponent implements OnInit {

  constructor(
      private downloader: Downloader,
  ) { }

  // Download a file
  download() {
  }

/*  // Downloading method to get Android APK
  download() {
    const request: DownloadRequest = {
      uri: '/builds/Nefaden/Nivantis_App/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk',
      title: 'MyDownload',
      description: '',
      mimeType: '',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
        dirType: 'Downloads',
        subPath: 'Nivantis.apk'
      }
    };

    this.downloader.download(request)
        .then((location: string) => console.log('File downloaded at:' + location))
        .catch((error: any) => console.error(error));
}*/
  ngOnInit() {}
}
