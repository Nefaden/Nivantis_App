import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-net-selling-price-popover',
  templateUrl: './net-selling-price-popover.component.html',
  styleUrls: ['./net-selling-price-popover.component.scss'],
})
export class NetSellingPricePopoverComponent implements OnInit {

    constructor() {
        this.netSellingPrice = '–– €';
    }

    multiplier: number;
    netPurchasePrice: number;
    netSellingPrice: string;

    ngOnInit() {}

    computeNetSellingPrice(event) {
        if (this.netPurchasePrice != null && this.multiplier != null) {
            const temp = this.netPurchasePrice * this.multiplier;
            this.netSellingPrice = temp.toFixed(2) + ' €';
        }
    }

}
