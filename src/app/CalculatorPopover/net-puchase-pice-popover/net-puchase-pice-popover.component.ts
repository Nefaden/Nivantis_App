import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-net-puchase-pice-popover',
  templateUrl: './net-puchase-pice-popover.component.html',
  styleUrls: ['./net-puchase-pice-popover.component.scss'],
})
export class NetPuchasePicePopoverComponent implements OnInit {

    constructor() {
        this.netPurchasePrice = '–– €';
    }

    discountRate: number;
    netPurchasePrice: string;
    grossPurchasePrice: number;

    ngOnInit() {}

    computeNetPurchasePrice(event) {
        if (this.grossPurchasePrice != null && this.discountRate != null) {
            const temp = this.grossPurchasePrice * (1 - this.discountRate / 100);
            this.netPurchasePrice = temp.toFixed(2) + ' €';
        }
    }

}
