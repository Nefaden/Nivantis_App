import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multiplier-popover',
  templateUrl: './multiplier-popover.component.html',
  styleUrls: ['./multiplier-popover.component.scss'],
})
export class MultiplierPopoverComponent implements OnInit {

    constructor() {
        this.multiplier = '–– ×';
    }

    multiplier: string;
    netPurchasePrice: number;
    netSellingPrice: number;

    ngOnInit() {}

    computeMultiplier(event) {
        if (this.netPurchasePrice != null && this.netPurchasePrice !== 0 && this.netSellingPrice != null) {
            const temp = this.netPurchasePrice / this.netSellingPrice;
            this.multiplier = temp.toFixed(2) + ' ×';
        }
    }

}
