import { Component, OnInit } from '@angular/core';
import { NumericValueAccessor, PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-discount-rate-popover',
  templateUrl: './discount-rate-popover.component.html',
  styleUrls: ['./discount-rate-popover.component.scss'],
})
export class DiscountRatePopoverComponent implements OnInit {

  constructor(private popoverController: PopoverController) {
      this.discountRate = '–– %';
  }

  discountRate: string;
  netPurchasePrice: number;
  grossPurchasePrice: number;

  ngOnInit() {}

  computeDiscountRate(event) {
    if (this.grossPurchasePrice > 0 && this.netPurchasePrice > 0) {
        const temp = (1 - (this.netPurchasePrice / this.grossPurchasePrice) ) * 100;
        this.discountRate = temp.toFixed(2) + ' %';
    }
  }

  closePopover() {
      this.popoverController.dismiss();
  }
}
