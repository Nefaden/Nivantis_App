import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GeolocationPage } from './geolocation.page';

import { GeolocationPopoverComponent } from '../geolocationPopover/geolocationPopover.component';

const routes: Routes = [
  {
    path: '',
    component: GeolocationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [GeolocationPopoverComponent],
  declarations: [GeolocationPage, GeolocationPopoverComponent]
})
export class GeolocationPageModule {}
