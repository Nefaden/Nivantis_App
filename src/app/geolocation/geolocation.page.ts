import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {GeolocationPopoverComponent} from '../geolocationPopover/geolocationPopover.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-geolocalisation',
  templateUrl: './geolocation.page.html',
  styleUrls: ['./geolocation.page.scss'],
})
export class GeolocationPage implements OnInit {

  data = '';
  constructor(
      private popoverController: PopoverController,
      private geolocation: Geolocation
  ) {
  }

  ngOnInit() {
    console.log('The component is initialized');
  }

  async geolocationPopover() {
    const popover = await this.popoverController.create({
      component: GeolocationPopoverComponent,
      cssClass: 'popover_class',
    });
    return await popover.present();
  }

    locate() {
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      const myQuery = [{
        latitude: resp.coords.latitude,
        longitude: resp.coords.longitude
      }];
      console.log(myQuery);
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    });
  }
}
