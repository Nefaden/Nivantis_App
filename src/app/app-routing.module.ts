import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes =
[
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'calculator',
    loadChildren: './calculator/calculator.module#CalculatorPageModule'
  },
  { path: 'geolocation',
    loadChildren: './geolocation/geolocation.module#GeolocationPageModule'
  },
  { path: 'pharmaceuticals',
    loadChildren: './pharmaceuticals/pharmaceuticals.module#PharmaceuticalsPageModule'
  },
  { path: 'visits',
    loadChildren: './visits/visits.module#VisitsPageModule'
  },
  { path: 'formations',
    loadChildren: './formations/formations.module#FormationsPageModule'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
