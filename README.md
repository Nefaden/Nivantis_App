# Conception, développement et intégration d’une Solution Applicative
[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![devDependencies Status](https://david-dm.org/nefaden/Nivantis_App/dev-status.svg)](https://david-dm.org/nefaden/Nivantis_App?type=dev) 
![build status](https://gitlab.com/nefaden/Nivantis_App/badges/master/build.svg) 
[![NPM Version](https://img.shields.io/npm/v/npm.svg?style=flat)]()
[![NPM Downloads](https://img.shields.io/npm/dt/express.svg?style=flat)]()
[![coverage report](https://gitlab.com/nefaden/Nivantis_App/badges/master/coverage.svg)](https://github.com/nefaden/Nivantis_App/commits/master) 
[![Discord Chat](https://img.shields.io/discord/308323056592486420.svg)](https://discord.gg/b5H7yht)

**Maintenance et développement d’une solution applicative de qualité**

- Conception d’une application en mettant en œuvre le développement piloté par les tests :
    - Tests gérer et effectuer via gitlab-ci
    - Déploiement de l'application sur une instance Scaleway
    - l'API C# sera déployé sur une seconde instance
- Mise en œuvre des principaux design patterns afin de répondre à une problématique donnée : Singleton, MVC
- Garantir la conformité aux standards internes et externes : normes d’utilisabilité, de performance, de fiabilité et de compatibilité

**Concevoir une solution applicative mobile** 

- Développement d’une application cross-platform à l’aide du framework Ionic (Angular et Cordova)
- Développement des composants d’interface tout en participant à l’élaboration d’interfaces utilisateur : design, ergonomie, interactivité
- Développement des composants d’accès aux données via SQL Server

**Développement d’une application N-Tier**

- Développement des composants métier
- Construire une application organisée en couches
- Composer une solution s’appuyant sur plusieurs protocoles adaptés
- Analyser une architecture logicielle existante afin de formuler des recommandations pour une éventuelle refonte
- Création d'une API coder en C# pour la connection à la base de données stocké sur SQL Server et déployer sur Azure
    - Lien vers l'API : https://gitlab.com/Nefaden/nivantis_api

**Clients des applications**

- DMO : Délégués Médecins Officines ou simplement délégué médical
- VM : visiteur médical, équivalent au DMO. 

visite les médecins spécialistes comme généraliste afin de leur présenter, pour le compte des laboratoire pharmaceutiques, les spécialités qu’ils produisent.

# Réalisation V1 : Calculatrice et Outil d’optimisation des achats d’une pharmacie

Afin de mieux répondre aux besoins des pharmaciens, Nivantis souhaite mettre en place une application PC/Android à destination des DMO (Délégués Médecins Officines). Nivantis souhaite un outil modulable permettant dans une première version de calculer facilement des prix d’achat, des prix de vente, des coefficients et des taux de remises pour le pharmacien

**interface intuitive**

- Taux de remise = (1 – Prix d’achat net / Prix d’achat brut) x 100
- Prix d’achat net = Prix d’achat brut x (1 – taux de remise)
- Prix de vente net = prix d’achat net x coefficient multiplicateur
- Coefficient multiplicateur = Prix de vente net / Prix d’achat net

**EVOLUTION** : vers un lien avec la bdd

# Réalisation V2 : Ajout de données des Pharmacies avec la géolocalisation

Dans un second temps, l’outil doit être capable de fournir des données importantes au DMO sur le pharmacien à partir d’une base clients mise à jour régulièrement (ainsi que de la géolocalisation du DMO).
ensemble de données sur la pharmacie à proximité (géolocalisation + identification manuelle si absence de réseau) : les achats par produit de la pharmacie, les ventes, les besoins en formation…

# Réalisation V3 : Mise en place de formulaire et recueil des données

Enfin, la mise en place et la collecte d’informations provenant de formulaires à remplir par le DMO doivent être possibles dans l’application :

- informations sur les officines
- questions à choix multiples ou des questions ouvertes
- format JSON
- téléchargeable
- création et mise à jour des formulaires facilité (hard mais on peut réaliser un utilitaire de création)

-----------------------------------------------------------------------------------------------------------------------------

A noter que l’application doit pouvoir être facilement mise à jour par Nivantis (micro-services) et doit-être compatible PC / Android.

# Installation du projet 

**INSTALLATION DES DÉPENDANCES**

Après avoir cloner le projet, il faut exécuter ces commandes :

- $ npm update
- $ npm install ionic cordova

Il est possible d'exécuter des commandes pour installer des plugins externes.
Documentation Ionic : https://ionicframework.com/docs/cli

# Exécution du projet

**EXÉCUTER ET TESTER LE PROJET**

Pour exécuter le projet :

- $ ng serve 

Permet d'exécuter le projet sur un serveur local

- $ ng lint | ng e2e | ng test

Permet d'exécuter les tests nécessaires 

- $ ng build <platform> 

Pemet de build le projet, possibilité de préciser la plateforme où build (android et iOS)